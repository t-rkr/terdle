![](terdle.gif)

# TERMinal worDLE

Simple Perl scripts for self-hosted Wordle-like instances that allow you to play in a terminal.


1. Put a terdle-server.pl call in your crontab to automatically generate new words, e.g.:

``0 0 * * * [user] /path/to/terdle-server /output/location``

This writes a word to a file at a given location which can be reached by `curl` or `wget`

2. Use the terdle-client.pl script to access the word of the day (`wotd`) at a given URL, e.g.:

``terdle-client.pl https://my.web.page``

3. Play the game as you normally would 

## References 
This script uses the word lists from 

[https://github.com/tabatkins/wordle-list](https://github.com/tabatkins/wordle-list)

[https://github.com/yyx990803/vue-wordle](https://github.com/yyx990803/vue-wordle)

